<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhraseTranslation extends Model
{

    public $timestamps = false;
    protected $fillable = ['word'];
}
