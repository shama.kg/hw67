<?php

namespace App\Models;


use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Phrase extends AbstractModel implements TranslatableContract
{
    use Translatable;

    protected $fillable = ['user_id', 'general'];

    public $translatedAttributes = ['word'];
}
