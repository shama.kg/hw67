<?php

namespace App\Http\Controllers;

use App\Models\Phrase;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;

class PhraseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $phrases = Phrase::all();
        $local = app()->getLocale();
        return view('phrases.index', compact('local', 'phrases'));
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'general' => 'required|min:3|max:255|regex:#^[а-яё\s]+$#iu',
        ]);
        $validated['user_id'] = auth()->user()->id;
        $validated['ru'] = ['word' => $validated['general'] ];
        Phrase::create($validated);
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     */
    public function show(Phrase $phrase)
    {
        return view('phrases.show', compact('phrase'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Phrase $phrase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Phrase $phrase)
    {
        $this->authorize('create-phrase');
        $validated = $request->validate([
            'en' => 'max:255',
            'kg' => 'max:255',
            'tr' => 'max:255',
            'de' => 'max:255',
            'es' => 'max:255',
        ]);

    $phrase->update($validated);
        return redirect()->route('home');
    }

}
