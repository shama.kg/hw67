<?php

use App\Http\Controllers\PhraseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::middleware('lang-switcher')->group(function(){
    Route::get('/', [PhraseController::class, 'index'])->name('home');
    Route::resource('phrases', PhraseController::class);

    Auth::routes();

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
    Route::get('lang/{locale}', [\App\Http\Controllers\LangSwitchController::class, 'switcher'])
        ->name('lang.switcher')
        ->where('locale', 'en|ru');
});
