<div class="form-floating">
    <select class="form-select" id="floatingSelect" aria-label="Floating label select example"
    onchange="location = this.value;"
    >
        @foreach($locales as $locale)
        <option value="{{route('lang.switcher',['locale'=>$locale])}}"
        {{$selected($locale)}}>
            {{__("message.".$locale)}}</option>
        @endforeach
    </select>
    <label for="floatingSelect">{{__('Language')}}</label>
</div>
