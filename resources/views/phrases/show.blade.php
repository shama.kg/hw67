@extends('layouts.app')

@section('content')
    @include('notifications.alerts')
    <h2>{{$phrase->general}}</h2>
    <div class="row">
        <div class="col-6">
            <form action="{{route('phrases.update', ['phrase'=> $phrase])}}" method="post">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{__('English')}}</label>
                    <input @if(!auth()->check()) disabled="disabled" @endif value="{{$phrase->translate('en')->word ?? ''}}" type="text" class="form-control" id="exampleInputPassword1" name="en[word]">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{__('Kyrgyz')}}</label>
                    <input @if(!auth()->check()) disabled="disabled" @endif value="{{$phrase->translate('kg')->word ?? ''}}  " type="text" class="form-control" id="exampleInputPassword1" name="kg[word]">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{__('Turkish')}}</label>
                    <input @if(!auth()->check()) disabled="disabled" @endif value="{{$phrase->translate('tr')->word ?? ''}}" type="text" class="form-control" id="exampleInputPassword1" name="tr[word]">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{__('Germany')}}</label>
                    <input @if(!auth()->check()) disabled="disabled" @endif value="{{$phrase->translate('de')->word ?? ''}}" type="text" class="form-control" id="exampleInputPassword1" name="de[word]">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">{{__('Spanish')}}</label>
                    <input @if(!auth()->check()) disabled="disabled" @endif value="{{$phrase->translate('es')->word ?? ''}}" type="text" class="form-control" id="exampleInputPassword1" name="es[word]">
                </div>
                @if(auth()->check())<button type="submit" class="btn btn-success">Сохранить</button>@endif
            </form>
        </div>
    </div>

    @endsection
