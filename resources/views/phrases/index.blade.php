@extends('layouts.app')

@section('content')
    @include('notifications.alerts')
    <h3>{{__("Local is : ").$local}}</h3>
    <h1 class="text-center">{{__('Translators workshop')}}</h1>
    <div class="row">
        <div class="col">
            <h2>{{__('Phrases')}}</h2>
            @foreach($phrases as $word)
                <a href="{{route('phrases.show', ['phrase' => $word])}}">
                    <p>{{ $word->general}}</p>
                </a>

            @endforeach
        </div>
    </div>
    @can('create-phrase')
    <div class="footer border p-5 rounded-2 border-3    ">
        <h3>{{__('Add phrase for translate')}}</h3>
        <form action="{{route("phrases.store")}}" method="post">
            @csrf
            <label for="word"></label>
            <input type="text" id="word" name="general">
            <button type="submit" class="btn btn-success">{{__('Add')}}</button>
        </form>
    </div>
    @endcan
@endsection

