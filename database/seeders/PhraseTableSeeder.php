<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PhraseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
//        \App\Models\User::all()->each(function ($user) {
//            $phrase_count = rand(5, 15);
//            \App\Models\Phrase::factory()
//                ->for($user)
//                ->count($phrase_count)->create();
//        });
        \App\Models\Phrase::factory()->count(5)->create();
    }
}
