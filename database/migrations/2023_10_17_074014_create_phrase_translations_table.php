<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('phrase_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('phrase_id')->constrained()->cascadeOnDelete();
            $table->string('locale')->index();
            $table->string('word')->nullable();
            $table->unique(['phrase_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('phrase_translations');
    }
};
