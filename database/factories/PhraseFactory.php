<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Phrase>
 */
class PhraseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => rand(1,3),
            'general' =>'НА Русском - ' . $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'en' => [
                'word' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            ],
            'kg' => [
                'word' => 'Кыргызча - ' . $this->faker->sentence($nbWords = 6, $variableNbWords = true),

            ],
            'tr' => [
                'word' => 'ПЕРЕВОД НА Турецкий - ' . $this->faker->sentence($nbWords = 6, $variableNbWords = true),

            ],
            'de' => [
                'word' => 'ПЕРЕВОД НА Немецкий - ' . $this->faker->sentence($nbWords = 6, $variableNbWords = true),

            ],
            'es' => [
                'word' => 'ПЕРЕВОД НА Испанский - ' . $this->faker->sentence($nbWords = 6, $variableNbWords = true),

            ],

        ];

    }
}
